import 'dart:convert';

import 'package:barcode_generator/to_print.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

import 'package:csv/csv.dart';

class LoadCSV extends StatefulWidget {
  const LoadCSV({Key? key}) : super(key: key);

  @override
  _LoadCSVState createState() => _LoadCSVState();
}

class _LoadCSVState extends State<LoadCSV> {
  List<List<dynamic>> _data = [];
  List<List<dynamic>> listData = [];

  // This function is triggered when the floating button is pressed
  void _loadCSV() async {
    final _rawData = await rootBundle.loadString("assets/kindacode.csv");
    List<List<dynamic>> _listData =
        const CsvToListConverter().convert(_rawData);
    setState(() {
      _data = _listData;
      listData = _listData;
    });
  }

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _loadCSV());
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController search = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        // The search area here
        title: Container(
          width: double.infinity,
          height: 40,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(5)),
          child: Center(
            child: TextFormField(
              controller: search,
              onFieldSubmitted: (value) {
                setState(() {
                  _data = search_list(search.text, listData);
                  // print(listData);
                });
              },
              decoration: InputDecoration(
                  prefixIcon: IconButton(
                    icon: const Icon(Icons.search),
                    onPressed: () {
                      setState(() {
                        if (listData == []) {
                          _loadCSV();
                        }
                        _data = search_list(search.text, listData);
                        // print(listData);
                      });
                    },
                  ),
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.clear),
                    onPressed: () {
                      search.text = '';
                    },
                  ),
                  hintText: 'Search...',
                  border: InputBorder.none),
            ),
          ),
        ),
      ),
      // Display the contents from the CSV file
      body: ListView.builder(
        itemCount: _data.length,
        itemBuilder: (_, index) {
          String code_intern = _data[index][0].toString();
          String libelle = _data[index][1].toString().length > 50
              ? _data[index][1].toString().substring(0, 50)
              : _data[index][1].toString();
          String EAN = _data[index][6].toString();
          String prix = _data[index][2].toString();
          String qte = '0';
          return Card(
            margin: const EdgeInsets.all(3),
            color: Colors.white,
            child: ListTile(
              leading: Text(code_intern),
              title: Wrap(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: libelle.length > 50
                          ? Text(libelle.substring(0, 50))
                          : Text(libelle.toString()),
                    ),
                    Text(prix)
                  ],
                ),
              ]),
              subtitle: Text(EAN),
              trailing: IconButton(
                icon: Icon(Icons.arrow_circle_right),
                onPressed: () {
                  added_prints.add([
                    code_intern.length > 12
                        ? (code_intern.substring(0, 12))
                        : (code_intern.toString()),
                    libelle.length > 30
                        ? (libelle.substring(0, 30))
                        : (libelle.toString()),
                    prix,
                    EAN,
                    '1'
                  ]);
                  print(added_prints);
                  setState(() {
                    // print_list.add(
                    //     '{B,1,N,$qte|\n1,"$code_intern"|\n2,"$libelle"|\n3,"$prix"|\n4,"$EAN"|\n}');
                    counter.value = added_prints.length;
                    print(counter);
                  });
                },
              ),
            ),
          );
        },
      ),

      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add), onPressed: _loadCSV),
    );
  }
}

List<List<dynamic>> search_list(String search, List<List<dynamic>> datum) {
  List<List<dynamic>> res; //= jsonDecode(jsonEncode(datum));
  res = datum.map((element) => List.from(element)).toList();
  search = search.toLowerCase();
  print(search);
  res.retainWhere((element) {
    String all_el = element.join(' ');
    all_el = all_el.toLowerCase();
    return all_el.contains(search);
  });
  // print(res);
  return res;
}
