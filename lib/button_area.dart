import 'package:barcode_generator/to_print.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:open_file/open_file.dart';
import 'package:file/local.dart';
import 'package:shell/shell.dart';
import 'package:process_run/which.dart';

class ButtonArea extends StatefulWidget {
  const ButtonArea({super.key});

  @override
  State<ButtonArea> createState() => _ButtonAreaState();
}

class _ButtonAreaState extends State<ButtonArea> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 600,
        height: 100,
        color: Colors.yellow,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red,
                ),
                onPressed: () {
                  setState(() {
                    print_list = [print_header];
                    added_prints = [
                      ['0', '1', '2', '3', '4']
                    ];
                    counter.value = added_prints.length;
                  });
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [Icon(Icons.delete), Text('Tout supprimer')],
                )),
            Expanded(
              child: ElevatedButton(
                child: Text('Imprimer'),
                onPressed: () {
                  print('Button pressed');
                  write_files();
                  setState(() {});
                },
              ),
            ),
          ],
        ));
  }
}

void write_files() async {
  final myDir = Directory('C:/barcode_generator');
  final scriptBat = 'SCRIPT.bat';
  final scriptFtp = 'scriptftp.txt';
  final printingFile = 'Print3825.txt';
  var isThereDir = await myDir.exists();
  var isThereFTP = await File(myDir.path + '/' + scriptFtp).exists();
  var isThereBat = await File(myDir.path + '/' + scriptBat).exists();
  File? batFile;
  if (isThereDir) {
    print('Directory exists');
  } else {
    var directory =
        await Directory('C:/barcode_generator').create(recursive: true);
    print(directory.path);
  }
  if (isThereFTP) {
    print('FTP exists');
  } else {
    var FTPFile = await File(myDir.path + '/' + scriptFtp)
        .writeAsString('open 192.168.0.10\n\naccess\nput Print3825.txt\nbye');
    print(FTPFile.path);
  }
  if (isThereBat) {
    print('BAT File exists');
  } else {
    batFile = await File(myDir.path + '/' + scriptBat)
        .writeAsString('@echo off\nftp -s:scriptftp.txt');
    print(batFile.path);
  }
  var printFile =
      await File(myDir.path + '/' + printingFile).writeAsString(add_to_file());
  print(printFile.path);

  // var shell = Shell();

  // await shell.run('''

  //   echo "hello world" > C:/dir/hello.txt

  // ''');

  // var shell = new Shell();
  // var fs = const LocalFileSystem();
  // var echo = await shell.start('echo', arguments: ['hello world']);
  // await echo.stdout.writeToFile(fs.file('hello.txt'));
  // await echo.stderr.drain();

  // OpenFile.open("C:/barcode_generator/SCRIPT.bat");

  Process.run('cmd.exe', ['/c', 'ftp -s:scriptftp.txt'],
          runInShell: false, workingDirectory: 'C:/barcode_generator')
      .then((ProcessResult results) {
    print(results.stdout);
  });

  print_list = [print_header];
  added_prints = [
    ['0', '1', '2', '3', '0']
  ];
  counter.value = added_prints.length;
}
