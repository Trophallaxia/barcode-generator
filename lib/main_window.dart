import 'package:barcode_generator/button_area.dart';
import 'package:barcode_generator/explore_area.dart';
import 'package:barcode_generator/print_preview_area.dart';
import 'package:barcode_generator/search_area.dart';
import 'package:flutter/material.dart';

class MainWindow extends StatefulWidget {
  const MainWindow({super.key});

  @override
  State<MainWindow> createState() => _MainWindowState();
}

class _MainWindowState extends State<MainWindow> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        alignment: Alignment.center,
        width: 1280,
        height: 720,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SearchArea(),
                  Expanded(child: ExploreArea()),
                ],
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Expanded(child: PrintPreviewArea()),
                  ButtonArea(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
