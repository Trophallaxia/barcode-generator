import 'package:flutter/material.dart';

String print_header =
    '{F,1,A,T,M,250,380,"E3825"|\nT,1,13,V,200,180,1,1,1,1,B,L,0,0,1|\nT,2,50,V,160,20,0,2,1,1,B,L,0,0,1|\nT,3,13,V,120,180,1,1,1,1,B,L,0,0,1|\nB,4,13,F,50,40,7,2,57,7,L,0|\n}';

List<String> print_list = [print_header];

List<List<String>> added_prints = [
  ['0', '1', '2', '3', '0']
];

int print_list_length = 1;

ValueNotifier<int> counter = ValueNotifier<int>(1);

String add_to_preview(int i) {
  if (i == 0) {
    return ('{F,1,A,T,M,250,380,"E3825"|\nT,1,13,V,200,180,1,1,1,1,B,L,0,0,1|\nT,2,50,V,160,20,0,2,1,1,B,L,0,0,1|\nT,3,13,V,120,180,1,1,1,1,B,L,0,0,1|\nB,4,13,F,50,40,7,2,57,7,L,0|\n}');
  } else {
    String str =
        '{B,1,N,${added_prints[i][4]}|\n1,"${added_prints[i][0]}"|\n2,"${added_prints[i][1]}"|\n3,"${added_prints[i][2]}"|\n4,"${added_prints[i][3]}"|\n}';

    return str;
  }
}

String add_to_file() {
  String str;
  int i = 1;
  str = print_header;
  while (i < added_prints.length) {
    str = str + "\n\n";
    str = str +
        '{B,1,N,${added_prints[i][4]}|\n1,"${added_prints[i][0]}"|\n2,"${added_prints[i][1]}"|\n3,"${added_prints[i][2]}"|\n4,"${added_prints[i][3]}"|\n}';

    i += 1;
  }
  return str;
}
