import 'package:barcode_generator/to_print.dart';
import 'package:flutter/material.dart';

class PrintPreviewArea extends StatefulWidget {
  const PrintPreviewArea({super.key});

  @override
  State<PrintPreviewArea> createState() => _PrintPreviewAreaState();
}

class _PrintPreviewAreaState extends State<PrintPreviewArea> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 600,
        height: 600,
        color: Color.fromARGB(255, 231, 223, 225),
        child: ValueListenableBuilder<int>(
          builder: (BuildContext context, int value, Widget? child) {
            // This builder will only get called when the _counter
            // is updated.
            return ListView.builder(
              itemCount: added_prints.length,
              itemBuilder: ((context, index) {
                return Column(
                  children: [
                    Container(
                      color: Colors.blue,
                      height: 20,
                      width: 600,
                      child: index == 0
                          ? Text('')
                          : Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                // Text(added_prints[index][1]),

                                // ElevatedButton(
                                //     onPressed: (() {
                                //       setState(() {
                                //         int nb = int.parse(added_prints[index][4]);
                                //         nb += 100;
                                //         added_prints[index][4] = nb.toString();
                                //       });
                                //     }),
                                //     child: Text('+100')),
                                // ElevatedButton(
                                //     onPressed: (() {
                                //       setState(() {
                                //         int nb = int.parse(added_prints[index][4]);
                                //         nb += 50;
                                //         added_prints[index][4] = nb.toString();
                                //       });
                                //     }),
                                //     child: Text('+50')),
                                // ElevatedButton(
                                //     onPressed: (() {
                                //       setState(() {
                                //         int nb = int.parse(added_prints[index][4]);
                                //         nb += 10;
                                //         added_prints[index][4] = nb.toString();
                                //       });
                                //     }),
                                //     child: Text('+10')),

                                TextButton(
                                    onPressed: (() {
                                      setState(() {
                                        int nb =
                                            int.parse(added_prints[index][4]);
                                        nb = nb > 1 ? nb - 1 : nb;
                                        added_prints[index][4] = nb.toString();
                                      });
                                    }),
                                    child: Text(
                                      '-1',
                                      style: TextStyle(color: Colors.white),
                                    )),
                                Text('Quantité : '),
                                Text(added_prints[index][4]),
                                TextButton(
                                    onPressed: (() {
                                      setState(() {
                                        int nb =
                                            int.parse(added_prints[index][4]);
                                        nb += 1;
                                        added_prints[index][4] = nb.toString();
                                      });
                                    }),
                                    child: Text(
                                      '+1',
                                      style: TextStyle(color: Colors.white),
                                    )),
                                // ElevatedButton(
                                //     onPressed: (() {
                                //       setState(() {
                                //         int nb = int.parse(added_prints[index][4]);
                                //         nb -= 10;
                                //         added_prints[index][4] = nb.toString();
                                //       });
                                //     }),
                                //     child: Text('-10')),
                                // ElevatedButton(
                                //     onPressed: (() {
                                //       setState(() {
                                //         int nb = int.parse(added_prints[index][4]);
                                //         nb -= 50;
                                //         added_prints[index][4] = nb.toString();
                                //       });
                                //     }),
                                //     child: Text('-50')),
                                // ElevatedButton(
                                //     onPressed: (() {
                                //       setState(() {
                                //         int nb = int.parse(added_prints[index][4]);
                                //         nb -= 100;
                                //         added_prints[index][4] = nb.toString();
                                //       });
                                //     }),
                                //     child: Text('-100')),

                                // ConstrainedBox(
                                //   constraints:
                                //       BoxConstraints(maxHeight: 20, maxWidth: 20),
                                //   child: TextFormField(
                                //     decoration: InputDecoration(
                                //       border: OutlineInputBorder(),
                                //       hintText: '0',
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                    ),
                    ListTile(
                      title: Text(add_to_preview(index)),
                      trailing: IconButton(
                        onPressed: () {
                          if (index != 0) {
                            setState(() {
                              added_prints.removeAt(index);
                            });
                          } else {}
                          ;
                        },
                        icon: Icon(Icons.delete),
                      ),
                    ),
                  ],
                );
              }),
            );
          },
          valueListenable: counter,
        )

        // Text(print_list.join('\n\n')),

        );
  }
}
